#!/bin/bash

IMG="$1"
DEV="$2"

if [[ -z "$(command -v growpart)" || -z "$(command -v btrfs)" ]]; then
	echo "growpart oder btrfs fehlt! Installiere cloud-guest-utils und btrfs-progs..."
	sudo apt update
	sudo apt install -y cloud-guest-utils btrfs-progs
fi

if [ -z "$IMG" ] ; then
echo "Image-File angeben!"
exit 1
fi

if [ -z "$DEV" ] ; then
echo "HDD dev angeben! z. B. /dev/hdc"
exit 1
fi

echo "Warte 5 Sekunden vor dem löschen, abbrechen mit strg + c..."
sleep 5
echo "Achtung - vorbei, ich mache jetzt alles platt auf" "${DEV}"

sudo dd if="$IMG" of=$DEV bs=1M status=progress
sync
sudo growpart $DEV 3
sudo mount  ${DEV}3 /mnt
sudo btrfs filesystem resize max /mnt
sudo umount /mnt
sync
