!/bin/bash
# ##############
#  Nettes Skript, das eine Datei via http holt
#  erwartet wird eine Liste von image-Dateien
#  zeilenweise getrennt
#  im gleichen Verzeichnis sollten sie liegen
# ################################

BASISURL="http://ct-pc/images"
INDEXURL="$BASISURL/index"

index=$(wget -qO- $INDEXURL)
echo $index
imgs=()
for element in $index; 
do
    imgs+=("$element")
    imgs+=("")
done

os=`dialog --menu "Abbild wählen" 0 0 0 \
 "${imgs[@]}" 3>&1 1>&2 2>&3`
dialog --clear


# Demonstriert dialog mit Auswahl

GG=`lsblk -r -p -n -oNAME`
echo $GG
Devs=()
for dev in $GG; do
    Devs+=($dev)
    Devs+=("")
done


echo ${Devs[@]}


dev=`dialog --menu "Festplattengerät wählen" 0 0 0 \
 "${Devs[@]}" 3>&1 1>&2 2>&3`
dialog --clear
dialog --yesno "Bestätigen Sie Ihre Auswahl: $dev" 0 0
dialog --clear




