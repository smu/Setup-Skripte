Dialogbasierte Imager-Skripte

Einfaches Zeug mit dd.

Einlesen:
Bereite die Platte vor. Die dritte Partition sollte / sein und btrfs haben
Verkleinere die Partition mit kparted so weit wie möglich.
Wähle die betreffende Platte aus. Nutze dabei das gesamt-Device
z.B. /dev/sdc. 
Wähle dann einen Dateinamen.

Das Skript ermittelt den Endsektor der letzten Partition (z.B. /dev/sdc3) und erstellt ein
Image indem es dd verwendet.


Ausschreiben:
dlg_platte_beschreiben.sh

einfach aufrufen, sudo-Rechte werden bei Bedarf erfragt.
Der erste Dialog fragt nach dem Device auf das geschrieben werden soll.
Wähle die angeschlossene SSD aus, raw. Nicht die Partition. Also /dev/sde z.B.
Der nächste Dialog lässt das Image auswählen. 
Wähle es aus und bestätige. Achtung: 'dialog' möchte die Selektion mit <Leertaste>.
Nach einer Sicherheisfrage geht es los.
Es dauert eine weile und die Platte ist bespielt.
Zum Schluss wird die letzte Partition auf die Plattengröße expandiert.
