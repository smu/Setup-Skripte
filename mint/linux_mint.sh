#!/usr/bin/env bash

# Execute itself with sudo if user is not root.
[[ $UID -ne 0 ]] && exec sudo ./"$0"

###
# Set variables
###
export DEBIAN_FRONTEND=noninteractive
DPKG_CFG_FRAGMENT=/etc/dpkg/dpkg.cfg.d/non-interactive

# Name of the distribution
script_distro='Linux Mint 22'
script_version='2.3.3'

# Additional packages
packages=(
  # Program packages
  libreoffice
  keepassxc
  gparted
  hardinfo
  regionset
  cheese
  vlc

  # Language related packages
  # Generic
  openoffice.org-hyphenation
  # AR
  language-pack-gnome-ar-base
  firefox-locale-ar
  fonts-arabeyes
  hunspell-ar
  libreoffice-l10n-ar
  mythes-ar
  thunderbird-locale-ar
  # DE
  language-pack-gnome-de-base # Installed only with Internet connection during the OEM setup.
  firefox-locale-de
  hunspell-de-at
  hunspell-de-ch
  hunspell-de-de
  thunderbird-locale-de # Installed only with Internet connection during the OEM setup.
  # EN
  thunderbird-locale-en-gb
  # FA
  language-pack-gnome-fa-base
  firefox-locale-fa
  libreoffice-l10n-fa
  myspell-fa
  thunderbird-locale-fa
  # RU
  language-pack-gnome-ru-base
  firefox-locale-ru
  thunderbird-locale-ru
  # UK
  language-pack-gnome-uk-base
  libreoffice-l10n-uk
  firefox-locale-uk
  thunderbird-locale-uk
  hyphen-uk
  mythes-uk
  hunspell-uk
  wukrainian
)

###
# Functions
###

# $*: message to echo.
e() {
  printf "\e[32m>>>\e[0m %s\n" "$*"
}

# It's a pain to stop execution (CTRL+C, kill) with all the Python applications so we catch SIGINT and SIGTERM and exit immediately.
handle_signal() {
  e "Ausführung abgebrochen! Skript wird beendet."
  exit 1
}

# Always executed when exiting the shell, regardless of the reason.
handle_exit() {
  e "Finale Aufräumarbeiten werden durchgeführt …"
  rm -f "$DPKG_CFG_FRAGMENT"
  rm -f /etc/apt/apt.conf.d/01aptproxy

  e "Initiales Setup beendet."
}

trap handle_signal SIGINT SIGTERM
trap handle_exit EXIT

pkg() {
  apt install "$@" --yes --quiet
}

###
# Greeting
###
echo "\
#####################################
# Computertruhe-Installationsskript #
#####################################

###
# Maintainer: Computertruhe e. V.
# Website:    https://computertruhe.de/
# Version:    ${script_version}
# Repo:       https://codeberg.org/Computertruhe/Setup-Skripte
# Distro:     ${script_distro}
###
"
e "Starte initiales Setup für Rechner mit frisch installiertem '${script_distro}' …"

# Use APT proxy if available.
ping -c 1 apt-proxy > /dev/null 2>&1
if [ "$?" -eq "0" ]; then
	e "Nutze APT-Proxy."
	echo 'Acquire::https::Proxy "http://apt-proxy:3142";' > /etc/apt/apt.conf.d/01aptproxy
	echo 'Acquire::http::Proxy "http://apt-proxy:3142";' >> /etc/apt/apt.conf.d/01aptproxy
else
	e "Kein APT-Proxy gefunden."
fi
echo 'Dpkg::Options "--force-unsafe-io";' >> /etc/apt/apt.conf.d/01aptproxy


###
# Automatic installation
###
# Place temporary dpkg configurations to ensure non-interactive upgrade.
cat <<'DPKG' >"$DPKG_CFG_FRAGMENT"
force-confold
force-confdef
DPKG

e "Paketquellen aktualisieren …"
apt update --yes --quiet

e "System aktualisieren (apt) …"
apt full-upgrade --yes --quiet

e "System aktualisieren (mintupdate-cli) …"
mintupdate-cli --yes upgrade

e "Proprietäre Treiber installieren (sofern verfügbar) …"
ubuntu-drivers autoinstall

e "Multimedia-Codecs installieren …"
pkg mint-meta-codecs # Installed only with Internet connection during the OEM setup.

e "Zusätzliche Software installieren …"
pkg "${packages[@]}"

e "Unnötige Pakete entfernen und Cache bereinigen …"
apt clean --yes --quiet
apt autoremove --yes --quiet

e "Handbuch \"Linux Mint – Handbuch der Computertruhe für Ein- und Umsteiger*innen\" herunterladen …"
mkdir /etc/skel/Desktop
wget -O "/etc/skel/Desktop/Linux Mint – Handbuch der Computertruhe für Ein- und Umsteiger*innen.pdf" https://computertruhe.de/handbuecher/linux_mint.pdf

e "Handbuch \"Besonderheiten von MacBooks im Zusammenhang mit Linux Mint\" herunterladen …"
if [ $(dmidecode -t1 | grep -ci "macbook") -ge 1 ]
then
	wget -O "/etc/skel/Desktop/Besonderheiten von MacBooks im Zusammenhang mit Linux Mint.pdf" https://computertruhe.de/handbuecher/macbook_linux_mint.pdf
else
	e "Das Skript läuft nicht auf einem MacBook."
fi
